# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository contains 3 files: 
HZ1.php :  Which contains the php code for inserting and retrieving the data into the database.
HelloZot.html: Which contains the HTML code for the form used.
testkaushal.sql: Which is the exported database file containing the data.

* Version: 1.0
### How do I get set up? ###

* Summary of set up : Once the setup is complete, the user will be able to enter the username and also see all the previous users login.
* Configuration: php and html files are stored in the htdocs folder of Xampp. The sql file is stored into the database folder peer to htdocs.
* Dependencies: Will require Xampp server to run using localhost.
* Database configuration: The database contains a table called 'username' which contains a single column called 'fname'. The username for mysql is 'root' and password is '' (null).
* How to run tests: Open the HTML file and enter your name. On submitting, the list of all the users can be observed. 


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

Any queries?
Kaushal Shah: 9222185628